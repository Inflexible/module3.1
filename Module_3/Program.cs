﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        protected Program() { }
        static void Main(string[] args)
        {
            Task1 task1 = new Task1();
            Console.WriteLine("Enter the first number: ");
            int num1 = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine("Enter the second number: ");
            int num2 = task1.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine(task1.Multiplication(num1, num2));

            Task2 task2 = new Task2();
            for (int i = 0; i < 2; i++)
            {
                Console.Write("Enter the natural number: ");
                if (task2.TryParseNaturalNumber(Console.ReadLine(), out int result))
                {
                    foreach (var item in task2.GetEvenNumbers(result))
                    {
                        Console.WriteLine(item);
                    }

                    return;
                }
                else
                {
                    Console.WriteLine("Input is not valid");
                }
            }

            Task3 task3 = new Task3();
            for (int i = 0; i < 2; i++)
            {
                Console.Write("Enter the natural number: ");
                if (task3.TryParseNaturalNumber(Console.ReadLine(), out int result))
                {
                    Console.Write("Enter digit you want to remove: ");
                    Console.WriteLine("New number: " + task3.RemoveDigitFromNumber(result, Convert.ToInt32(Console.ReadLine())));
                    return;
                }
                else
                {
                    Console.WriteLine("Input is not valid");
                }
            }

        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source, out int number))
            {
                return number;
            }
            else
            {
                throw new ArgumentException("Input is not valid");
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int sum = 0;
            for (int i = 1; i <= Math.Abs(num2); i++)
            {
                sum += Math.Abs(num1);
            }
            return ((num1 < 0 && num2 < 0) || (num1 > 0 && num2 > 0)) ? sum : -sum;
            }
    }

    public class Task2
    {
        public bool TryParseNaturalNumber(string input, out int result)
        {
            return int.TryParse(input, out result) && result >= 0;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> evenNumbers = new List<int>();
            for (int i = 0; i < naturalNumber * 2; i++)
            {
                if (i % 2 == 0)
                {
                    evenNumbers.Add(i);
                }
            }
            return evenNumbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            return int.TryParse(input, out result) && result >= 0;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string result = source.ToString();
            int index = result.IndexOf(digitToRemove.ToString());
            while (index != -1)
            {
                result = result.Remove(index, 1);
                index = result.IndexOf(digitToRemove.ToString());
            }
            return result;
        }
    }
}
